#definition a new class: phone

class Phone:
    #first method definitng how phones send calls
    def send_call(self):
        print("Sending call...")
        
    # second method definitiong how phones receive calls
    def recieve_call(self):
        print("Receive call...")
        
    #Third method definiting how phones send text messages
    def send_text_message(self):
        print("Sending text...")
        
    #Fourth method definiting how phones receive text messages
    def recieve_message(self):
        print("Receiving text message...")
        
#creating the first phone
phone1 = Phone()
phone2 = Phone()
phone3 = Phone()
    
    #sending a call using the first phone
phone1.send_call()
phone2.recieve_call()
phone3.send_text_message()
phone3.recieve_message()

#definition a new class: phoneP

class Phone_P:
    #first method definitng how phones send calls
    def send_call(self, number):
        print(f"Sending call to {number}...")
        
    # second method definitiong how phones receive calls
    def recieve_call(self, number):
        print(f"Receive call from {number}...")
        
    #Third method definiting how phones send text messages
    def send_text_message(self, number):
        print(f"Sending text to {number}...")
        
    #Fourth method definiting how phones receive text messages
    def recieve_message(self, number):
        print(f"Receiving text message from {number}...")
        
#creating the first phone methods with parameters
phone1 = Phone_P()
phone2 = Phone_P()
phone3 = Phone_P()

phone_number = "+352621289676"
    
    #sending a call using the first phone
phone1.send_call(phone_number)
phone2.recieve_call(phone_number)
phone3.send_text_message(phone_number)
phone3.recieve_message(phone_number)

#class constructor
class Phone_C:
        def __init__(self, brand, owner):
            self.brand = brand
            self.owner = owner
            self.battery_status = 1.0
            print(f"Congratulations {owner}! You have a new {brand} phone!")
    
        #first method definitng how phones send calls
        def send_call(self, number):
            print(f"(self.owner)'s (self.brand) is Sending call to {number}...")
        
        # second method definitiong how phones receive calls
        def recieve_call(self, number):
            print(f"(self.owner)'s (self.brand) Receives call from {number}...")
        
        #Third method definiting how phones send text messages
        def send_text_message(self, number):
            print(f"(self.owner)'s (self.brand) is sending text to {number}...")
        
        #Fourth method definiting how phones receive text messages
        def recieve_message(self, number):
            print(f"(self.owner)'s (self.brand) is receiving text message from {number}...")
    
#creating the first phone methods with parameters
phone1 = Phone_C("Samsung","Pierre")
phone2 = Phone_C("Apple", "Anne")
phone3 = Phone_C("Nokia", "Mimi")

phone_number = "+352621289676"
    
    #sending a call using the first phone
phone1.send_call(phone_number)
phone2.recieve_call(phone_number)
phone3.send_text_message(phone_number)
phone3.recieve_message(phone_number)
