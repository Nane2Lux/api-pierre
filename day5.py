# decorators ( functions)
# function computing a full name from a first name and a last name

#def full_name(first_name, last_name):
#    return first_name + " " + last_name
"""function taking a function func as an argument
    and commuting another function
    the new functiontakes a lit of couples of strings,
    computes the result of fun on every couple,
    then joins them using a line break "/" """
    
"""def list_names(func):
    def inner(str_couples):
        return "\n".join([func(val[0], val[1]) for val in str_couples])
    return inner

#full_name is transformed to a new function
# using list_names

full_name = list_names(full_name)

players_list = [("Antoire","Dupont"), ("Dan", "Carter"), ("Damien", "Poitrenault"), ("Thibault", "Flament")]

print(full_name(players_list))"""

# another way
# decorators ( functions)
# function computing a full name from a first name and a last name

"""function taking a function func as an argument
    and commuting another function
    the new functiontakes a lit of couples of strings,
    computes the result of fun on every couple,
    then joins them using a line break "\n" """
    
def list_names(func):
    def inner(str_couples):
        return "\n".join([func(val[0], val[1]) for val in str_couples])
    return inner
    
#@list_names is called decorator
#It transforms a function into a new function
#it is equivalent to : full_name = list_names(full_name)
#list_names

@list_names

def full_name(first_name, last_name):
    return first_name + " " + last_name


#full_name is transformed to a new function
# using list_names

players_list = [("Antoire","Dupont"), ("Dan", "Carter"), ("Damien", "Poitrenault"), ("Thibault", "Flament"), ("Aaron", "Smith"),("Dan", "Biggard")]

print(full_name(players_list))
