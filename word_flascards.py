class Flashcard:
    def __init__(self, word, meaning):
        self.word = word
        self.meaning = meaning
        
    def __str__(self):
       return f'Flashcard ({self.word}, {self.meaning})'

flash = []

print("Welcome to the creation of Flashcards")

stop = " "
while stop != "0":
    word = input("Enter your word: ")
    meaning = input("Enter the meaning of your word :")
    
    flash.append(Flashcard(word, meaning))
    stop = input("Press 0 to stop or keep doing: ")
    
#flashcard1 = Flashcard(input("the word : "), input("the meaning :"))
#flashcard = Flashcard('easy','facile')
#print(flashcard1)
#print(flashcard)

print("\nYour flashcards")
for i in flash:
    print(">", i)