# another way
# decorators ( functions)
# function computing a full name from a first name and a last name

"""Write a Python program to make a chain of function
décorators italic bold underline
Bold <b> & </b>
Italic <i> & </i>
Underline <u> & </u>"""
    
def bold_writing(func):
    def sentence():
        return "<b>" + func() +"</b>"
    return sentence

def italic_writing(func):
    def sentence():
        return "<i>" + func() +"</i>"
    return sentence

def underline_writing(func):
    def sentence():
        return "<u>" + func() +"</u>"
    return sentence
    
#@list_names is called decorator
#It transforms a function into a new function
#it is equivalent to : full_name = list_names(full_name)
#list_names

@bold_writing
@italic_writing
@underline_writing
def writing():
    return "Antoire Dupont is a rugbyman"

#full_name is transformed to a new function
# using list_names

print(writing())
